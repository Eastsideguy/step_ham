$(document).ready(function () {

    // function filterGallary
    $('.amazing-pics-item').hide().slice(0, 12).show();
    $('.btn-load-more').click(function () {
        const imageType = $(".filter-tabs.active").data('filter');
        $('.loader').css('display', 'flex');
        $('.btn-load-more').hide();
        setTimeout(function () {
            $(`.amazing-pics-item${imageType}:hidden`).slice(0, 12).show();
            $('.loader').hide();
            $('.btn-load-more').show();
        }, 2000);
        if (this.classList.contains('one-click')) {
            $('.loader').css('display', 'flex');
            $('.btn-load-more').hide();
            setTimeout(function () {
                $('.btn-load-more').hide();
            }, 2000);
        }
        $(this).addClass('one-click');
    });
    $('.filter-tabs').click(function () {
        $('.btn-load-more').show().removeClass('one-click');
        $(this).addClass('active').siblings().removeClass('active');
        const imageType = $(this).data("filter");
        $(".amazing-pics-item").hide();
        $(`.amazing-pics-item${imageType}`).slice(0, 12).show();

    });


    //function switchTab

    $('.tabs-btn').click(function () {
        let tab_id = $(this).data('tab');
        $('.tabs-btn').removeClass('active');
        $('.service-description').removeClass('active');
        $(this).addClass('active');
        $("#" + tab_id).addClass('active');

    });

    // function slickSlider

    $('.slider-top').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-bot'
    });

    $('.slider-bot').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-top',
        prevArrow: '.left-slide',
        nextArrow: '.right-slide',
        dots: false,
        arrows: true,
        focusOnSelect: true,
        centerMode: true,
        centerPadding: '40px',
    });

});